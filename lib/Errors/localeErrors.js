module.exports = {
    "MissingArgError": {
        "it_IT.UTF-8": "Argomento non fornito",
        "def": "Argument not provided"
    },

    "WrongArgTypeError" : {
        "it_IT.UTF-8": "Type errato per l'argomento",
        "def": "Wrong type for argument"
    }
}