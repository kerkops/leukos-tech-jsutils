/** 
 * Errori relativi agli argomenti di una funzione 
 * */


const LANG = process.env.LANG || "def";


// console.log(`LANG IS ${LANG}`)
const locale = require('./localeErrors');

/** Un argomento necessario alla funzione non è stato fornito */
class MissingArgError extends Error {
    constructor(argName) {
        let message = `${locale.MissingArgError[LANG]||locale.MissingArgError['def']}: \`${argName}\``;
        super(message)
        
        this.name = "MissingArgError";
        this.argName = argName;
        
    }
};

/** Un argomento fornito è del tipo sbagliato */
class WrongArgTypeError extends Error {
    /**
     * 
     * @param {String} argName Nome dell'argomento errato
     * @param {Any} value Il valore fornito
     * @param {Any} expected Il valore/i atteso per quell'argomento
     */
    constructor (argName, value, expected) {
        
        let message = `${locale.WrongArgTypeError[LANG]||locale.WrongArgTypeError['def']} \`${argName}\`: ${value} (${typeof value}). Exp.: ${expected}.`;

        super(message);

        this.name = "WrongArgTypeError";
        this.argName = argName;
        this.value = value;
        this.expected = expected;
        
    }
};


/** Errori relativi agli argomenti di una funzione */
module.exports = {
    WrongArgTypeError: WrongArgTypeError,
    MissingArgError: MissingArgError
}