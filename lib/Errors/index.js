/** 
 * Classi di errore personalizzate da utilizzare nei progetti Leukos-Tech 
 * 
 * `.args` - Errori relativi agli argomenti di una funzione
 * 
 * */

const arguments = require('./arguments_errors');



module.exports = {
    /** @property Errori relativi agli argomenti */
    args: arguments
};