/** Funzioni dedicate allo unittesting */
const timeIt = require('./timeit.js');

const DEBUG_ENABLED = (['1', 'true'].includes(process.env.DEBUG)) ? true : false

/** @constant {Number} HEADER_SPACE Lunghezza in char per le stringhe grafiche degli header */
const HEADER_SPACE = 150;

/** @constant {Number} PADDING Lunghezza della porzione di riempimento sul lato sinistro (testo allineato a SX + PADDING) */
const PADDING = 10;

/** @constant {Number} DELTA_MULTIPLIER Moltiplicatore per il parametro `delta` di `getHeaderString()`. Il numero di caratteri rientrati è uguale a `delta * DELTA_MULTIPLER`. */
const DELTA_MULTIPLIER = 2;


/**
 * Genera una stringa grafica con l'header al centro e una serie di carateri ai lati.
 * 
 * @param {String} headerString Stringa da utilizzare come *header*
 * @param {String} delimiter Carattere da utilizzare al principio e fine della stringa generata (Delimitatore)
 * @param {String} spacer Carattere da utilizzare per riempire la stringa ai lati dell'header 
 * @param {Number} delta Numero di char di rientro laterale (default=0) 
 * @param {String} extra Caratteri extra da aggiungere al termine della stringa (generalmente `\n` per spaziature verticali)
 * 
 * @return {String} Stringa da utilizzare come header per le funzioni `describe()` di *unittesting* 
 * 
 * **Se `process.env.DEBUG` è `true` aggiunge un `\n` all'inizio della stringa per aumentare la spaziatura verticale**
 */
function getHeaderString(headerString, delimiter, spacer, delta=0, extra='') {
  let verSpacing = DEBUG_ENABLED ? '\n' : '';
  let spacerLen = (HEADER_SPACE - headerString.length ) - PADDING;
  return `\n${verSpacing}${String(" ".repeat(delta*DELTA_MULTIPLIER))}  ${delimiter}${spacer.repeat(PADDING-(delta*DELTA_MULTIPLIER))} ${headerString} ${spacer.repeat(spacerLen-(delta*DELTA_MULTIPLIER))}${delimiter}${extra}${verSpacing}`;
};

/**
 * Restituisce una stringa grafica per l'header fornito, in base al preset selezionato
 * @param {String} header Stringa con l'header
 * @param {Number} level Identificativo del tipo di formattazione (default=2)
 * 
 * * 0 --> `|====== <header> ===========================|`
 * * 1 ---> `:----- <header> --------------------------:`
 * * 2 ----> `----- <header> --------------------------` [DEFAULT]
 * * 3 ----> `~~~~~ <header ~~~~~~~~~~~~~~~~~~~~~~~~~~~` 
 * * 4 ---> `¦~~~~~ <header> ~~~~~~~~~~~~~~~~~~~~~~~~~~¦`
 * * 5 --> `§~~~~~~ <header> ~~~~~~~~~~~~~~~~~~~~~~~~~~~§`
 * 
 * @return {String} Stringa da utilizzare come header per le funzioni `describe()` di *unittesting* 
 * 
 * **Se `process.env.DEBUG` è `true` aggiunge un `\n` all'inizio della stringa per aumentare la spaziatura verticale**
 * 
 */
function formatHeader(header, level=2) {
  switch (level) {
    case 0:
      return getHeaderString(header, '|', "=", level, "\n\n");
    case 1:
      return getHeaderString(header, ':', '-', level, "\n");
    case 3: 
        return getHeaderString(header, '~', '~', 2, '\n');
    case 4: 
        return getHeaderString(header, '¦', '~', 1, '\n');
    case 5: 
        return getHeaderString(header, '§', '~', 0, '\n');
    default:
      return getHeaderString(header, '-', '-', level)
  }
};




module.exports = {
  getHeader: formatHeader,
  getHeaderString: getHeaderString,
  timeIt: timeIt
}