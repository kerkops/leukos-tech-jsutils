/** 
 * UTILITA' PER LE FUNZIONI DI `JSON`.
 * 
 * */

""

/** REGOLE DI SANITIZZAZIONE UTILIZZATE DALLA FUNZIONE `safeJsonData()` */
const SANITY_KEYS = {
    /** Chiavi relative alle password */
    PASSWORD_KEYS : ['password','pwd', 'passkey', 'passphrase']
}

/**
 * Filtro da fornitre come 2° argomento alla funzione `JSON.stringify(object, replacer)` quando l'oggetto contiene dati sensibili come ad esempi le **password**.
 *
 * @param {String} key Chiave dell'oggetto
 * @param {any} value Valore
 * @returns {any} Il valore originale se esso non fa parte delle regole di esclusione, altrimenti il valore impostato nelle regole
 */
function safeJsonData (key, value) {

    /** Regole di esclusione
     *  Se il campo è 'password' restituisce '*******'
     *  
     */    
    if (SANITY_KEYS.PASSWORD_KEYS.includes(key)) {
        return "***********";
    };

    

    // In caso le regole di esclusione non si applichino viene restituito il valore intonso.
    return value
};


module.exports = {
    SANITY_KEYS: SANITY_KEYS,
    safeJsonData: safeJsonData
}
