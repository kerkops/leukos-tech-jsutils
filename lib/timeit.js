const fs = require('fs');
const os = require('os');
const _log = require('./log');
const logStr = _log.logString;
const errStr = _log.errString;

require('./types_extensions')
/** Directory di destinazione dei test */
const TIMEIT_DUMP_DIR = `${os.homedir()}/Leukos-Tech-TimingResults`;

const DEBUG_ENABLED = (['1', 'true'].includes(process.env.DEBUG)) ? true : false

/** @constant {Number} HEADER_SPACE Lunghezza in char per le stringhe grafiche degli header */
const HEADER_SPACE = 150;

// /** @constant {Number} PADDING Lunghezza della porzione di riempimento sul lato sinistro (testo allineato a SX + PADDING) */
// const PADDING = 10;

/** @constant {Number} DELTA_MULTIPLIER Moltiplicatore per il parametro `delta` di `getHeaderString()`. Il numero di caratteri rientrati è uguale a `delta * DELTA_MULTIPLER`. */
const DELTA_MULTIPLIER = 2;

/** Numero di chiamate alla funzione per stabilire il tick più basso */
const INIT_CALLS = 100;

/** Divisore per portare il risultato delle chiamate a process.hrtime() in millisecondi */
const MS_DIVIDER = 1000000000;
/**
 * 
 * @param {String} header Header per il test (es. 'TCxxx')
 * @param {Array} args Argomenti utilizzati per la chiamata
 * @param {*} time 
 * @param {*} ticks 
 * @param {*} spacer 
 * @param {*} left_del 
 * @param {*} right_del 
 */
function getTimingString(header, args, time, ticks, spacer=' ', left_del = ' ', right_del = ' ') {
    return `${String(" ".repeat(DELTA_MULTIPLIER))}${left_del} ${getDumpTimingString(header, args, time, ticks)}`;
};
  
function getDumpTimingString(header, args, time, ticks) {
    nameSpaceLeft = (HEADER_SPACE/2)-header.length;
    let _argsStr = Array(args).join(', ');
  
    return `${header}${ String(" ".repeat(nameSpaceLeft))} Exc Time: ${time} (Ticks: ${ticks}) Args: ${_argsStr}`;
    
};

function getLowestTick() {
  let f = process.hrtime;
  let time = f();
  let delta = f(time);
  let results = [];
  // let lowest = reduceHrtime(delta);
  for (let i=0; i<INIT_CALLS; i++) {
    delta = f(f());

    // console.log(`delta: ${delta}`)
    results.push(reduceHrtime(delta));
  };
  // console.log(results)
  let lowest = Math.min(...results);
  // console.log(`Lowest: ${lowest}`)
  return lowest
}

/** 
 * Restituisce l'array prodotto da `process.hrtime()` in un valore `float` derivato dalla somma del primo elemento alla divisione per `MS_DIVIDER` del secondo
 * 
 * @returns {Number} Valore in secondi  
 * */
function reduceHrtime(hrTimeArray) {
  return hrTimeArray[0] + (hrTimeArray[1]/MS_DIVIDER)
};

class TimingResults {
    constructor(header) {

        
        /** @property Sub oggetto con le proprietà del sistema per i report */
        this.sysInfo = new SysInfo();
        this.init();
        this.header = header;
        
    };
    /**
     * Azzera le proprietà dell'oggetto contenenti le info sui test
     * Non azzeraa l'header fornito col costruttore
     */
    init() {
        /** @property Array a cui aggiungere i risultati delle misurazioni */
        this.res = new Array;

        /** @property Data dei test */
        this.date = new Date().toISOString();
        // Attributi specifici per giorno ed ora
        [this.day, this.time] = this.date.split('T');
        
        /** @property Intervallo minimo per una oprazione (serve a comparare i test tra loro al variare delle condizioni del sistema) */
        this.tick = getLowestTick();
        // console.log(`Tick: ${this.tick}`)

    }

    /**
     * Aggiunge una misurazione ai risultati
     * @param {String} header Signature della funzione; es. "myfunction(arg1, arg2, arg3)"
     * @param {Array} time Array di 2 elementi restituito dal metodo `process.hrtime()`
     * @param {Array} args Array degli argomenti utilizzati per la chiamata alla funzione 
     */
    add(header, time, args=[]) {
        let [sec, ms] = time;
        // console.log(`sec: ${sec}`)
        ms = ms / 1000000000;
        // console.log(`ms: ${ms}`)
        let _time = sec + ms;
        // console.log(`time: ${_time}`)

        // let _time = time[0]+(time[1]/1000000000);
        // Numero di tick per l'esecuzione
        let ticks = (_time / this.tick).toFixed(0);

        this.res.push({header: header, time: _time, args: args, ticks: ticks});

    };

    /**
     * Restituisce la string multilinea coi i rusultati, pronta per essere loggata o memorizzata
     */
    _toString() {
        let timeResStr = '';
        for (let call of this.res) {
          // console.log(`time: ${call.time}`)
            timeResStr += getTimingString(call.header, call.args, call.time, call.ticks);
            timeResStr += '\n\t';
        }
        return `
    ~~~~ RISULTATI TIMING (TICK = ${this.tick})~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\t${timeResStr}
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
`
//         return `
// ~~~~ SYSTEM DETAILS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ${this.sysInfo}
// ~~~~ RISULTATI TIMING (TICK = ${this.tick})~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ${timeResStr}
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// `
    };

    /**
     * Resituisce una stringa multilinea coi dati sul sistema e sulle misurazioni fino ad ora accumulate.
     * Utilizzare `getResults()` invece di questo.
     */
    toString() {
        return this._toString()
    };

    /**
     * Memorizza su disco il contenuto corrente dell'oggetto.
     * Utilizzare `getResults()` invece di questo.
     * 
     * @returns {Boolean} `true` se il file è stato memorizzato correttamente
     */
    save() {
        // Directory di base dei dump (in funzione del titolo)
        let currentDir = `${TIMEIT_DUMP_DIR}/${this.header}`;
        if (!fs.existsSync(TIMEIT_DUMP_DIR)) { 
          try {
            fs.mkdirSync(TIMEIT_DUMP_DIR, {recursive: true}); 
          } catch(err) {
            console.error(errStr(`IMPOSSIBILE CREARE LA DIRECTORY PER IL DUMP DEL TIMING:'${TIMEIT_DUMP_DIR}'`, err));
            return false;
          } 
      };
      if (!fs.existsSync(currentDir)) { 
            try {
              fs.mkdirSync(currentDir, {recursive: true}); 
            } catch(err) {
              console.error(errStr(`IMPOSSIBILE CREARE LA DIRECTORY PER IL DUMP DEL TIMING:'${currentDir}'`, err));
              return false;
            } 
        };
        let outputPath = `${currentDir}/${this.date}.txt`;
        try {
            fs.appendFileSync(outputPath, this._toString());
            return true;
        } catch (err) {
            console.error(errString(`IMPOSSIBILE LA SCRITTURA DEL FILE RISULTATI TIMING: '${outputPath}'`, err));
              return false;
        };

          
    

    };

    /**
     * Restituisce la stringa (multilinea) da loggare coi risultati
     * @param {Boolean} save Se `true` (*default*) memorizza su disco i risultati alla cartella `<homedir>\Leukos-Tech-TimingResults\<header>` ( con l'`header` fornito al costruttore)
     * @param {Boolean} reset Se `true` (*default*) azzera le proprietà dell'oggetto prima di restituire la stringa.
     * 
     * @returns {String} Stringa multilinea coi dati sul sistema e sulle misurazioni fino ad ora accumulate.
     */
    getResults(save=true, reset=true){
        if (save) {let saved = this.save()}

        let result = this._toString();
        if (reset) {this.init()}

        return result

    };
        
}

/**
 * Oggetto contenente informazioni sul sistema in esecuzione
 */
class SysInfo {
    constructor() {
        /** @property Architettura del sistema */
        this.arch = os.arch();

        /** @property Memoria disponibile in Gigabyte */
        this.mem = os.totalmem()/(1024*1024*1024);
        
        // Oggetto cono le info sulle cpu del sistema
        let cpus =  os.cpus();

        /** @property Array di stringhe, una per il modello di ogni processore montato */
        this.cpus = [];

        // Popola l'array con la proprietà `model` di ogni item dell'array
        for (let cpu of cpus) {
            if (!this.cpus.includes(cpu.model))
            this.cpus.push(cpu.model);
        };

        /** @property Stringa **multilinea** con le info sulla macchina in uso */
        this.machine_info = "";
        
        /** @property Numero di cpu disponibili */
        this.cpus_number = cpus.length;

        /** @property Hostname della macchina in uso */
        this.hostname = os.hostname();

        // Oggetto con le info sull'utente della macchina
        let userInfo = os.userInfo();

        /** @property Username dell'utente loggato */
        this.username = userInfo.username;

        /** @property Home dir dell'utente */
        this.homedir = userInfo.homedir;

        /** @property Stringa con le info sulla macchina (hostname, architettura) */
        this.machine_id = `HOSTNAME: ${this.hostname} ARCHITETTURA: (${this.arch})`;

        if (this.cpus.length==1) {
          this.machine_info = `RAM: ${this.mem.toFixed(2)} Gb
CPU: ${this.cpus[0]} (x${this.cpus_number})
`
        } else {
        this.machine_info = `RAM: ${this.mem.toFixed(2)} Gb
CPU: ${this.cpus_number}
- ${this.cpus.join('\n- ')}
`};
    };

    toString() {
        return `${this.machine_id}\n${this.machine_info}`;
    }
};


module.exports = TimingResults;
  

// let tr = new TimingResults('test_header');

// let time = process.hrtime()
// let total = process.hrtime(time)
// tr.add('test()', total,[])
// let time2 = process.hrtime();
// let total2 = process.hrtime(time2);
// tr.add('test(2)', total2, []);
// tr.add('test(3)', process.hrtime(time), []);
// tr.add('test(4)', process.hrtime(time2), [])

// console.log(tr.toString())

// // tr.save();

// console.log(tr.getResults(false));

// console.log(tr.toString())
