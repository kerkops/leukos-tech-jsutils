/** Utility legate all'oggetto `Error` */


/** 
 * Array di esempio ottenuto con `err.stack.split("\n"` in `stackParser()`)
  INDICE 0  ---> 'Error',
  INDICE 1  ---> '    at test_stackParser (/home/leukos/Repos/leukos-tech-jsutils/lib/errors_test.js:10:35)' ]
  INDICE 2  ---> '    at Object.<anonymous> (/home/leukos/Repos/leukos-tech-jsutils/lib/errors_test.js:24:1)',
  INDICE 3  ---> '    at Module._compile (module.js:652:30)',
  INDICE 4  ---> '    at Object.Module._extensions..js (module.js:663:10)',
  INDICE 5  ---> '    at Module.load (module.js:565:32)',
  INDICE 6  ---> '    at tryModuleLoad (module.js:505:12)',
  INDICE 7  ---> '    at Function.Module._load (module.js:497:3)',
  INDICE 8  ---> '    at Function.Module.runMain (module.js:693:10)',
  INDICE 9  ---> '    at startup (bootstrap_node.js:188:16)',
  INDICE 10 ---> '    at bootstrap_node.js:609:3'  
*/


/** 
 * @constant Primo indice utile nell'array di lavoro generato utilizzando il metodo `String.split()` sull'argomento di `stackParser()`.
 * 
 * Viene utilizzato come indice di `end` PER IL METODO `Array.splice(start, end)` (elimina e restituisce gli elementi dell'array compresi tra i 2 indici) per ottenere l'array di base di stringhe dello stack.
 * 
 * Viene poi utilizzato per *individuare l'item, nell'Array di lavoro di `stackParser()`, da cui estrapolare lineNumber, columnNumber e filename* dalla Array di lavoro per l'elemento indagato (indice) e per il chiamante (indice+1)
 * 
 * Elementi rispsetto all'indice:
 * 
 * - INDICE `0` - nome dell'errore
 * 
 * - INDICE `1` - info sulla chiamata alla funzione `stackParser()`
 * 
 * - INDICE `2` - info sulla chiamata alla funzione che ha poi chiamato stack parser.
*/
const stackParser_FIRST_MEANING_INDEX = 1;

/**
 * Restituisce lo stack corrente sollevano una eccezione fittizia
 *
 * @param {Number} meaningIndex Indice iniziali per i dati significativi (elimina quelli precedenti)
  
 * @return {Object} stackObj, con le proprietà:
 * 
 * * meaning {Object} Informazioni sulla funzione che ha richiesto lo stack
 * * caller {Object} Informazioni sulla funzione chiamante quella che ha richiesto lo stack
 * * lower {String} Stringa contenente il resto dello stack.
 * 
 * `.meaning` e `.caller` possiedono entrambi la seguente struttura:
 * 
 * * file {String} File ove risiede la funzione
 * * func {String} Nome della funzione
 * * line {Number} Numero di linea
 * * col {Number} Numro di colonna
 
 */
function getStack(meaningIndex=null) {
    try {
        throw new Error('## FLOW CONTROL ##')
    } catch (err) {
        let index = meaningIndex || stackParser_FIRST_MEANING_INDEX + 3;
        return stackParser(err.stack, index)
    }
}

/**
 * Esegue il parsing dell'attributo `Error.stack` restituendo un oggetto che contiene informazioni relativamente a la funzione corrente (`.meaning`), la funzione chiamante (`.caller`) e il resto dello stack (`.lower`).
 * 
 * @param {String} stack Stringa in cui è stato eseguito il `dump` dello stack
 * @param {Number} meaningIndex Indice iniziali per i dati significativi (elimina quelli precedenti)
 * 
 * @return {StackObject} `data`, con le proprietà:
 * 
 * * meaning {Object} Informazioni sulla funzione che ha richiesto lo stack
 * * caller {Object} Informazioni sulla funzione chiamante quella che ha richiesto lo stack
 * * lower {String} Stringa contenente il resto dello stack
 */
function stackParser (stack, meaningIndex = stackParser_FIRST_MEANING_INDEX) {
    let rawArray = stack.split("\n");
    let eliminated=rawArray.splice(0, meaningIndex); 
    // console.log(eliminated)
    let _meaning = rawArray.shift()
    let outArray = [];
    let _caller = rawArray.shift()
    let meaning = stackStringParser( _meaning );
    let caller = stackStringParser ( _caller );
    // console.log(meaning);
    // console.log(caller);
    // console.log(rawArray);

    for ( let i=0; i < (rawArray.length-1); i++) {outArray.push(stackStringParser(rawArray[i]))}
    return {
        meaning : meaning,
        caller: caller,
        lower: outArray
    }




};

/**
 * Effettua il parsing di una stringa dello stack
 * @param {String} stackString Stringa (ottenuta manipolando `Error.stack`) di cui eseguire il parsing
 * @returns {Object} `stackLevelData`, con le seguenti proprietà
 * 
 * * func {String}  Nome della funzione 
 * * file {String}  Nome del file 
 * * line {Number}  Numero di Riga 
 * * col {Number}  Numero di Colonna 
 */
function stackStringParser(stackString) {

    /** @constant {String} Porzione iniziale di stringa da eliminare (ripiazzata con "") */
    let initialPadding = "    at ";

    /** @constant {Array} Le stringhe (*chars*) in questo array vengono rimosse (sostituite con "") */
    let toEliminate = ['(', ')'];

    // Eredita i metodi delle stringhe (estesi da `types_extension`)
    stackString = String(stackString);

    // console.log(`STRINGA INIZIALE: \`${stackString}\``);
    
    // Elimina la porzione iniziale di stringa
    stackString = stackString.replaceAll(initialPadding, "");
    
    // console.log(`STRINGA DE-PADDED: \`${stackString}\``);
    
    // Elimina gli altri char non utili
    for (let toDel of toEliminate) { stackString = stackString.replaceAll(toDel, "") };

    // console.log(`STRINGA DE-CHARRED: \`${stackString}\``);
    
    // SPLIT IN BASE A " " --> [NOME_METODO, FILENAME:LINE:COL]
    let space_splitted = stackString.split(" ");

    // console.log(`SPACE SPLITTED: ${space_splitted}`);
    
    // SPLIT IN BASE A ":" --> [NOME_FILE, LINE, COL]
    // if (space_splitted)
    
    // Sorgente per la stringa da parsare sostituendo ":"
    let ptSrc;

    if (space_splitted.length == 1) {
        ptSrc = String(space_splitted);
    } else {
        ptSrc = String(space_splitted[1]);
    };
    
    // console.log(`PT SRC: ${ptSrc}`);


    let points_splitted = ptSrc.split(":");

    // console.log(`POINTS SPLITTED: ${points_splitted}`)
    
    return {

        /** @property {String} func Nome della funzione */
        func : space_splitted[0],
        
        /** @property {String} file Nome del file */
        file : points_splitted[0],
                
        /** @property {Number} line Numero di Riga */
        line : Number(points_splitted[1]),

        /** @property {Number} col Numero di Colonna */
        col : Number(points_splitted[2]),

        /** @property  {String} coordString Stringa originaria file+line+col (FASTER) */
        coordString: space_splitted[1]
    }

};

/**
 * Estrapola lo stack dell'oggetto fornito come argomento `err`
 * @param {Object} err Oggetto err di cui costruire le stringhe di errore
 * @param {Number} meaningIndex Indice significativo dello stack (elimina le parti prima che servono solo a fini di lavoro)
 * 
 * @returns {Object} `StackObject` con le seguenti proprietà:
 * 
 */
function errorStackParser(err,  meaningIndex = stackParser_FIRST_MEANING_INDEX) {


    return stackParser(err.stack, meaningIndex);
};





module.exports = {
    stackParser : stackParser,
    errStackParser : errorStackParser,
    getStack: getStack
};
