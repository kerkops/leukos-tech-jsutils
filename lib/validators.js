
/**
 * Verifica che l'oggetto fornito come primo argomento possieda le proprietà specificate nel secondo (verificando anche il type).
 * 
 * @param {Object} inspected Oggetto parametri da validare
 * @param {Object} cfrData Oggetto con i dati per la validazione la cui struttura deve adeguarsi al seguente formato:
 * 
 * `{ <nomeProprietàAttesa>: <typeAtteso>, <nomeProprietàAttesa>: <typeAtteso>, ... }`
 * 
 * @returns {Object} `result`
 * * `success` {Boolean} - `true` se i parametri sono validi.
 * * `problem` {String} - `"MISS"` per argomento mancante, `"WRONG"` per proprietà dal `type` errato.
 * * `propertyName` {String} - Se `success==false` contiene il nome della proprietà irregolare.
 * * `value` {Any} - Se `success==false` e `problem=="WRONG"` contiene il valore (errato) fornito per la proprietà `propertyName`.
 * * `expected` {Any} - Se `success==false` e `problem=="WRONG"` contiene il `type` del valore atteso.
 * 
 * 
 */
function validateObject(inspected, cfrData) {

    // Cicla le chiavi dell'oggetto che definisce lo schema corretto
    for ( let propertyName of Object.keys(cfrData) ) {

        // Alias per il type atteso della proprietà corrente
        let propertyType = cfrData[propertyName];

        // L'array di chiavi dell'oggetto da verificare non contiene la chiave corrente
        if (!Object.keys(inspected).includes(propertyName)) {
            // MissingArgError
            return {success:false, propertyName: propertyName, problem: "MISS"}
        };

        // Salta la verifica del type se `propertyType` è null
        if (!propertyType || propertyType==null) { continue; }

        // La proprietà esiste ma il `type` è sbagliato
        if (typeof inspected[propertyName] != propertyType) {
            return {success: false, propertyName: propertyName, problem: "WRONG", value: inspected[propertyName], expected: propertyType}
            // throw new argsErrs.WrongArgTypeError(propertyName, params[propertyName], propertyType);
        };
    };

    return {success:true};

    
    
};

module.exports.validateObject = validateObject;