/** FUNZIONI GENERICHE DI I-O DI OGGETTI (LETTURA, SCRITTURA, COPIA) */

const json = require('./json');

/**
 * Crea una copia indipendente dell'oggetto (json-parsable) fornito come argomento
 *
 * @param {Object} object Oggetto da copiare
 * @param {boolean} [safeJson=false] Se True, vengono applicati i filtri della funzione safeJsonData(), altrimenti l'oggetto viene copiato normalmente (default).
 * @returns {Object} La copia dell'oggetto
 */
function copyObj(object, safeJson=false) {
    
    let str = '';
    

    if (safeJson) {
        str = JSON.stringify(object, json.safeJsonData);
    } else {
        str = JSON.stringify(object);
    };

    return JSON.parse(str);
}


module.exports = {
    copyObj: copyObj,
    
}