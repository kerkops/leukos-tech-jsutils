// let re = /([^(]+)@|at ([^(]+) \(/g;
//     let aRegexResult = re.exec(new Error().stack);
//     sCallerName = aRegexResult[1] || aRegexResult[2];

const errors = require(`./errFunctions`);
// const LoggedClass = require(`./log.LoggedClass`);

const __FIELD_SEPARATOR__ = " - ";
const __DATE_SEPARATOR__ = " h ";
const __REGULAR_INCIPIT__ = " ==> "
const __ERROR_INCIPIT__ = " !!! "

/** 
 * Metodo di base per la creazione della stringa di logging
 * @param {String} message Messaggio di cui eseguire il logging
 * @param {Object} error Se fornito, aggiunge informazioni sull'errore, altrimenti ne genera uno fittizio per ricavare i dati
 * @param {Boolean} addTimestamp Se `true` (*default*) aggiunge il timestamp all'inizio della stringa
 * @param {Number} meaningIndex Indice iniziale da cui prelevare lo stack (ignora i precedenti)
 * 
 * @returns {String} La stringa pronta per essere passata al metodo del `logger`
 */
function _logString(message='', error=null, addTimestamp=true, meaningIndex=null) {


    let info, incipit, output;
    if (!error) {
        info = formatStackData (errors.getStack(meaningIndex).meaning);
        incipit = __REGULAR_INCIPIT__;
    } else {
        info = formatErrStackData( errors.errStackParser(error, meaningIndex).meaning, error);
        incipit = __ERROR_INCIPIT__;
    };

    let outMessage = (message != '') ? message : '';

    let timestamp = (addTimestamp) ? new Date().toISOString().replace('T', __DATE_SEPARATOR__) : "";

    output = `${timestamp}${incipit}${info}${__FIELD_SEPARATOR__}${message}`
    
    return output;
};

/** Restituisce i dati sullo stack forniti secondo la formattazione impostata */
function formatStackData(data) {
    // return `${data.file}[${data.line},${data.col}]${__FIELD_SEPARATOR__}${data.func}()`;
    return `${data.func}()[${data.line},${data.col}]`;
}

/** Restituisce i dati sullo stack con l'aggiunta del nome dell'errore e il suo argoment0 */
function formatErrStackData(data, err) {return `${formatStackData(data)}${__FIELD_SEPARATOR__}![${err.name}:${err.message}]!`}



/** 
 * Aggiunge al messaggio info utili al `logging` come il nome del file del modulo, il nome della funzione, il numero di riga, etc.
 * 
 * @param {String} message Messaggio di cui eseguire il logging
 * @param {Boolean} addTimestamp Se `true` (*default*) aggiunge il timestamp all'inizio della stringa
 * @param {Number} meaningIndex Indice iniziali per i dati significativi (elimina quelli precedenti)
 *
 *  @returns {String} La stringa pronta per essere passata al metodo del `logger`
 */
function logString(message, addTimestamp=true, meaningIndex=null) {
    return _logString(message, null, addTimestamp, meaningIndex);
};

/** 
 * Aggiunge al messaggio info utili al `logging` come il nome del file del modulo, il nome della funzione, il numero di riga, etc. e quelle di base dell'errore (`message` e `name`)
 * 
 * @param {String} message Messaggio di cui eseguire il logging
 * @param {Object} error Errore da cui ricavare le proprietà
 * @param {Boolean} addTimestamp Se `true` (*default*) aggiunge il timestamp all'inizio della stringa
 * @param {Number} meaningIndex Indice iniziale per i dati significativi (elimina quelli precedenti)
 * 
 * @returns {String} La stringa pronta per essere passata al metodo del `logger`
 */
function errString(message, err, addTimestamp=true, meaningIndex=null) {
    return _logString(message, err, addTimestamp, meaningIndex)
}



module.exports = {

    logString : logString,
    errString: errString,
    // LoggedClass: LoggedClass
}
    