const logUtils = require('../log');

// Indice dello stach per le funzioni di logging
const STACK_INDEX = 5;

/** @const {Object} _LOG_LEVELS Definizione della relazione tra stringhe indicanti i possibili livelli di logging e il corrispettivo valore numerico */
const _LOG_LEVELS = {
    debug : 0,
    info: 1,
    warn: 2,
    error: 3
};


/** @const {Array} _LEVEL_STRINGS Definizione delle stringhe di incipit per le righe da loggare, ordinate secondo il livello */
const _LEVEL_STRINGS = [
    "? DEBUG ?",
    "- INFO  -",
    "* WARN  *",
    "! ERROR !"
];

/**
 * Classe Ancestor per fornire a quelle che la ereditano funzionalità native di logging.
 * 
 * Ognuna delle classi child possiederà i metodi: 
 *  * `._debug(message)`
 *  * `._info(message)`
 *  * `._warn(message)`
 *  * `.error(message)`
 * 
 * Le classi che la ereditano devono prevedere, nel proprio costruttore, gli argomenti `debugEnabled` {**Boolean**}, `loggerInstance` {**Object**} (*opzionale*) e `loggerLevel` {**Number**} (*opzionale*)da fornire al costruttore di `LoggedClass`.
 */
class LoggedClass {

    /** @property Contiene i livelli di logging possibili */
    static get LOG_LEVELS() {return _LOG_LEVELS}

    /** @property Contiene l'incipit delle righe da loggare, ordinate secondo il livello */
    static get _LEVEL_STRINGS() {return _LEVEL_STRINGS}

    /**
     * Costruttore dell'Oggetto.
     * 
     * Genera un oggetto con gli attributi relativi al logging valorizzati e i metodi `_debug()`, `_info()`, `_warn()` e `_error()` operativi.
     * @param {Boolean} debugEnabled Se `true` attiva il logging delle informazioni addizionali tramite il logger fornito con l'arg `loggerInstance` o tramite la `console` se non fornito (*default* `false`).
     * @param {Object} loggerInstance Oggetto da utilizzare per il logging. Se non fornita la `console` verrà utilizzata. 
     * @param {Number} loggerLevel Livello minimo di cui eseguire il *logging* (*default* `LoggedClass.LOG_LEVELS.info` -> 1)
     */
    constructor(debugEnabled=false, loggerInstance=null, loggerLevel=_LOG_LEVELS.info) {


        /** @property {Boolean} Se True il logging viene attivato */
        this._debugEnabled = debugEnabled;

        /** @property {Object} Oggetto da utilizzare per il logging dei dati */
        this._logger = loggerInstance || console;

        /** @property {Number} Livello minimo per il logging delle informazioni (quelli inferiori vengono ignorati) */
        this._logLevel = loggerLevel;
    };


    /**
     * Funzione basica per il logging
     * 
     * Tramite questa funzione il logging avviene solo se *il debug è attivo* e se *il livello fornito come argomento è >= a quello impostato come livello minimo oppure se il livello è `error`.
     * 
     * @param {Number} level Livello di logging per il messaggio corrente
     * @param {String} message Messaggio di cui eseguire il logging
     */
    _log(level, message) {

        if ( (this._debugEnabled == true && level >= this._logLevel) || level == _LOG_LEVELS.error) {

            let loggerMethod = this._getLoggerMethod(level);

            loggerMethod(`${_LEVEL_STRINGS[level]} ${message}`)

            
        }
    };

    /**
     * Restituisce il metodo del logger in uso corrispondente al livello fornito come argomento
     * 
     * @param {Number} level Livello del logging secondo i valori nell'attributo *statico* `LoggedClass.LOG_LEVELS`
     * 
     * @returns {Object} Metodo dell'istanza del `logger` da utilizzare per il livello in uso
     */
    _getLoggerMethod(level) {
        switch (level) {
            case _LOG_LEVELS.debug:
                return this._logger.debug;
                break;
            case _LOG_LEVELS.warn:
                return this._logger.warn;
                break;
            case _LOG_LEVELS.error:
                return this._logger.error;
                break;
            default:
                return this._logger.info;
        }
    };

    /**
     * Esegue il logging del messaggio `message` tramite il `logger` attribuito alla classe, a livello `LoggedClass.LOG_LEVELS.debug`.
     * 
     * Funzione di convenienza per la chiamata a `._log(level, message)` col primo argomento già pre-impostato.
     * 
     * @param {String} message Messaggio da loggare a livello "DEBUG"
     */
    _debug(message) {
        this._log(_LOG_LEVELS.debug, logUtils.logString(message, true, STACK_INDEX));
    };

    /**
     * Esegue il logging del messaggio `message` tramite il `logger` attribuito alla classe, a livello `LoggedClass.LOG_LEVELS.info`.
     * 
     * Funzione di convenienza per la chiamata a `._log(level, message)` col primo argomento già pre-impostato.
     * 
     * @param {String} message Messaggio da loggare a livello "INFO"
     */
    _info(message) {
        this._log(_LOG_LEVELS.info, logUtils.logString(message, true, STACK_INDEX));
    };

    /**
     * Esegue il logging del messaggio `message` tramite il `logger` attribuito alla classe, a livello `LoggedClass.LOG_LEVELS.warn`.
     * 
     * Funzione di convenienza per la chiamata a `._log(level, message)` col primo argomento già pre-impostato.
     * 
     * @param {String} message Messaggio da loggare a livello "WARN"
     */
    _warn(message) {
        this._log(_LOG_LEVELS.warn, logUtils.logString(message, true, STACK_INDEX));
    };


    /**
     * Esegue il logging del messaggio `message` tramite il `logger` attribuito alla classe, a livello `LoggedClass.LOG_LEVELS.error`.
     * 
     * Funzione di convenienza per la chiamata a `._log(level, message)` col primo argomento già pre-impostato.
     * 
     * @param {String} message Messaggio da loggare a livello "DEBUG"
     */
    _error(err, message) {
        this._log(_LOG_LEVELS.error, logUtils.errString(message, err, true, 1));
    };
}


module.exports = LoggedClass;