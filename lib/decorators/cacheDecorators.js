/** 
 * Decoratori dedicata al caching dei metodi
 */



/**
 * Fornisce funzionalità di caching dei risultati della funzione decorata
 * @param {Function} func Funzione o metodo da decorare può essere fornito inserendo il decoratore con la notazione @cacheIt(enabled) prima della dichiarazione del metodo 
 * @param {Boolean} enabled 
 */
function cacheIt(func, enabled=true) {
    let cache = new Map();
    
    return function() {
        if (enabled) {
            // Applica il metodo Array.join all'array degli argomenti
            let key = [].join.call(arguments);

            // Se la chiave esiste restituisce il valore memorizzato
            if (cache.has(key)) return cache.get(key);

            // Esegue il metodo decorato
            let result = func.apply(this, arguments);

            // Memorizza il valore
            cache.set (key, result);

            return result;
        } else {
            func.apply(this, arguments);
        }


    }
};


module.exports.cacheIt = cacheIt;
