# Leukos-Tech-JsUtils

Insieme di utilità suddivise in categorie di applicazione.

Package per `npm`.

<a name='summary'></a> 

## Sommario

* [Installazione](#install)
* [Contenuto del package](#classes)
    * [leukos-tech.jsutils.io](#io)
        * **[copyObj(object, safeJson=false)](#copyobj)**
    * [leukos-tech.jsutils.json](#json)
        * **[safeJsonData(key, value)](#safejson)**
    * [leukos-tech-jsutil.log](#log)
        * **[logString(message, addTimestamp=true)](#logstring)**
        * **[errString(message, err, addTimestamp=true)](#errString)**
    * [leukos-tech.jsutils.errors](#errors)
        * [errStackParser(err, meaningIndex=1)](#errstackparser)
        * [stackParser(stack, meaningIndex=1)](#stackparser)
        * **[getStack()](#getstack)**
    * [leukos-tech-jsutils.ancestors](#ancestors)
        * **[LoggedClass - (class)](#loggedclass)**
    * [leukos-tech-jsutils.custErrors](#custerrors)
        * [args](#argscusterrors)
            * **[MissingArgError - (class)](#missingargerror)**
            * **[WrongArgTypeError - (class)](#wrongargtypeerror)**
    * [leukos-tech-jsutils.tests](#tests)
        * [getHeaderString(headerString, delimiter, spacer, delta, extra)](#getheaderstring)
        * **[getHeader(header, level)](#getheader)**
        * **[TimingResults - (class)](#timingresults)**
    * [leukos-tech-jsutils.decorators](#decorators)
        * **[cacheIt(method, enabled)](#cacheit)**
    * [leukos-tech-jsutils.validators](#validators)
        * **[validateObject(inspected, cfrData)](#validateobject)**
    * [Estensione dei *types* di base](#typeextensions)
        * [String](#stringte)
            * [String.prototype.replaceAll(str1, str2, ignore)](#stringreplaceall)
    
<a name='install'></a> 

## Installazione

```bash
$ npm install leukos-tech-jsutils --save
```

<br>




<a name='classes'></a> 

## Contenuto del package

Di seguito l'elenco degli oggetti contenuti nel package.

<br>

<a name='io'></a> 

## `leukos-tech-jsutils.io` 

Oggetto contenente funzioni dedicate all'I/O (lettura, scrittura, copia, etc)

<br>

<a name='copyobj'></a> 

### `leukos-tech-jsutils.io.copyObj( object {Object}, safeJson {Boolean} = false )`

Crea una copia dell'oggetto (json-parsable) fornito come 1° argomento

#### Argomenti

* `object` {**Object**} Oggetto da copiare (deve essere *json-parsable*).
* `safeJson` {**Boolean**} Se `false` (*default*) copia l'oggetto così come è. Altrimenti applica il filtro in `leukos-tech-jsutils.json.safeJsonData()` ([Vai alla documentazione](#safejson)).

#### Tipi Restituiti

* `copied` {**Object**} La copia dell'oggetto fornito, indipendente dall'oggetto originario (modificando i valori dell'uno l'altro rimane invariato).


#### Utlizzo:

```javascript
const {copyObj} = require('leukos-tech-jsutils').io;
// Require dell'oggetto

let independent_copy = copyObj(object_to_copy, false); 
// Restituisce una copia dell'oggetto

let independent_copy = copyObj(object_to_copy, true); 
// Restituisce una copia dell'oggetto in cui i campi sensibili sono stati alterati
```

[Torna al Sommario](#summary)

<br><br><br>

---

<a name='json'></a> 

## `leukos-tech-jsutils.json`

Oggetto contenente funzioni dedicate all'oggetto `JSON`.

<br>

<a name='safejson'></a>

### `leukos-tech-jsutils.json.safeJsonData( key {String}, value {any} )`

Filtro da utilizzare come 2° argomento per la funzione *JSON.stringify(object, replacer)*. Sanitizza proprietà dell'oggetto fornito come argomento contenenti dati sensibili (come le password).

#### Argomenti

* `key` {**String**} Chiave corrente

* `value` {**any**} Valore associato alla chiave, eventualmente da sanitizzare se la chiave è inclusa tra le regole di sanitizzazione.

#### Tipi restituiti

* `filteredValue` {**any**} Il valore intonso, se la chiave non ricade tra quelle da sanitizzare, o il valore sanitizzato.


#### Utilizzo

```javascript
const {safeJsonData} = require('leukos-tech-jsutils').json;

let cleanObj = JSON.stringify(some_object, safeJsonData);
```


#### Comportamento

Per campi identificati dalle chiavi:

* `password`
* `pwd`
* `passkey`
* `passphrase`

restituisce "\*\*\*\*\*\*\*\*\*\*\*" invece del valore originale

[Torna al Sommario](#summary)

<br><br><br>


---

<a name='log'></a> 

## `leukos-tech-jsutils.log`

Oggetto contenente funzioni di utilità per il logging

<br>

<a name='logstring'></a> 

### `leukos-tech-jsutils.log.logString(message {String}, addTimestamp {Boolean} = true)`

Aggiunge al messaggio info utili al `logging` come il nome del file del modulo, il nome della funzione, il numero di riga, etc.

#### Argomenti

* `message` {**String**} Messaggio da loggare

* `addTimestamp` {**Boolean**} Se `true` (*default*) aggiunge il timestamp a inizio della riga

#### Tipi restituiti

* `toLogger` {**String**} Stringa pronta per essere passata al logger.

[Torna al Sommario](#summary)

___


<a name='errstring'></a> 

### `leukos-tech-jsutils.log.errString(message {String}, err {Object}, addTimestamp {Boolean} = true)`

Aggiunge al messaggio info utili al `logging` come il nome del file del modulo, il nome della funzione, il numero di riga, etc. e quelle di base dell'errore (`message` e `name`)

#### Argomenti

* `message` {**String**} Messaggio da loggare

* `err` {**Object**} Derivato di `Error` da cui estrapolare `name` e `message`.

* `addTimestamp` {**Boolean**} Se `true` (*default*) aggiunge il timestamp a inizio della riga

#### Tipi restituiti

* `toLogger` {**String**} Stringa pronta per essere passata al logger.

[Torna al Sommario](#summary)

<br><br><br>

___

<a name='errors'></a> 

## `leukos-tech-jsutils.errors`

Contiene funzioni dedicate agli errori e alla loro gestione

<br>

### <a name='errstackparser'></a> `leukos-tech-jsutils.errors.errStackParser(err {Object}, meaningIndex {Number} = 1)`

Estrapola lo stack dell'oggetto fornito come argomento `err`. Wrapper di `errors.stackParser()`.

#### Argomenti

* `err` {**Object**} Oggetto da cui estrapolare la proprietà `.stack` {String}.

* `meaningIndex` {**Number**} Indice iniziale per i dati significativi da estrapolare dalla stringa `stack` (elimina quelli precedenti).mUna volta trasformato in array esegue il metodo `Array.splice(0, meaningIndex)` per rimuovere da esso dati spuri dovuti all'elaborazione.

#### Tipi Restituiti:

`stackData` {**StackObject**}, oggetto con le seguenti proprietà:

* `meaning` {**StackData**} Oggetto contenente le proprietà per il livello più alto della `stack` (secondo le proprietà `file`, `func`, `line`, `col`)

* `caller` {**StackData**} Oggetto contenente le proprietà del livello *caller* (secondo le proprietà `file`, `func`, `line`, `col`))

* `lower` {**Array**} Array di oggetti `StackData` a ritroso per la stack

[Torna al Sommario](#summary)

___

<a name='stackparser'></a> 

### `leukos-tech-jsutils.errors.stackParser(stack {String}, meaningIndex {Number} = 1)`

Effettua il parsing della stringa di stack fornita come argomento.

#### Argomenti

* `stack` {**String**} Stack di cui effettuare il parsing

* `meaningIndex` {**Number**} Indice iniziale per i dati significativi da estrapolare dalla stringa `stack` (elimina quelli precedenti). Una volta trasformato in array esegue il metodo `Array.splice(0, meaningIndex)` per rimuovere da esso dati spuri dovuti all'elaborazione.

#### Tipi Restituiti:

`stackData` {**StackObject**}, oggetto con le seguenti proprietà:

* `meaning` {**StackData**} Oggetto contenente le proprietà per il livello più alto della `stack` (secondo le proprietà `file`, `func`, `line`, `col`)

* `caller` {**StackData**} Oggetto contenente le proprietà del livello *caller* (secondo le proprietà `file`, `func`, `line`, `col`))

* `lower` {**Array**} Array di oggetti `StackData` a ritroso per la stack

[Torna al Sommario](#summary)

___

<a name='getstack'></a> 

### `leukos-tech-jsutils.errors.getStack()`

Restituisce l'oggetto `StackObject` dell'istante in cui viene chiamata sollevando una eccezione fittizia ed utilizzando su di essa la funzione `stackParser()`.

#### Tipi Restituiti:

`stackData` {**StackObject**}, oggetto con le seguenti proprietà:

* `meaning` {**StackData**} Oggetto contenente le proprietà per il livello più alto della `stack` (secondo le proprietà `file`, `func`, `line`, `col`)

* `caller` {**StackData**} Oggetto contenente le proprietà del livello *caller* (secondo le proprietà `file`, `func`, `line`, `col`))

* `lower` {**Array**} Array di oggetti `StackData` a ritroso per la stack


[Torna al Sommario](#summary)

<br><br><br>

___

<a name='ancestors'></a> 

## `leukos-tech-jsutils.ancestors`

Package di Classi *ancestor* da ereditare per implementare funzionalità specifiche negli oggetti.

<br>

<a name='loggedclass'></a> 

### `leukos-tech-jsutils.ancestors.LoggedClass`

Classe Ancestor per fornire a quelle che la ereditano funzionalità native di *logging*.
  
Ognuna delle classi *child* possiederà i metodi: 

 * `._debug(message)`
 * `._info(message)`
 * `._warn(message)`
 * `._error(message)`
 
  
Le classi che la ereditano devono prevedere, nel proprio costruttore, gli argomenti:

* `debugEnabled` {**Boolean**}
* `loggerInstance` {**Object**} (*opzionale*) 
* `loggerLevel` {**Number**} (*opzionale*) 

da fornire al costruttore di `LoggedClass` tramite 

```javascript
super(debugEnabled, loggerInstance, loggerLevel);
```
[Torna al Sommario](#summary)

<br><br><br>



___

<a name='custerrors'></a> 

## `leukos-tech-jsutils.custErrors`

Package di classi Errore personalizzate da utilizzare nei progetti `Leukos-Tech`.

<br>

<a name='argscusterrors'></a> 

## `leukos-tech-jsutils.custErrors.args`

Errori relativi agli argomenti delle funzioni

<br>

<a name='missingargerror'></a> 

### `leukos-tech-jsutils.custErrors.args.MissingArgError`

Un argomento necessario allo svolgimento della funzione *non è stato fornito*

#### Costruttore 

```javascript
new MissingArgError(argName {String})
```

[Torna al Sommario](#summary)

<br>

<a name='wrongargtypeerror'></a> 

### `lukos-tech-jsutils.custErrors.args.WrongArgTypeError`

Un argomento è stato fornito del tipo sbagliato

#### Costruttore 

```javascript
new WrongArgTypeError(argName{String}, value{Any}, expected{Any})
```

[Torna al Sommario](#summary)

<br><br><br>


___

<a name='tests'></a> 

## `leukos-tech-jsutils.tests`

Utilità focalizzate sullo *unittesting*.

<br>

### <a name='getheaderstring'></a> `leukos-tech-jsutils.tests.getHeaderString( headerString {String}, delimiter {Number}, spacer {String}, delta {Number}, extra {String} ): graphicString {String}`

Genera una stringa grafica con l'header sulla sinistra e una serie di carateri ai lati.

#### Argomenti

* `headerString` **{String}** Stringa da utilizzare come *header*
* `delimiter` **{String}** Carattere da utilizzare al principio e fine della stringa generata (Delimitatore)
* `spacer` **{String}** Carattere da utilizzare per riempire la stringa ai lati dell'`headerString` 
* `delta` **{Number}** Numero di char di rientro laterale (default=0) (Viene moltiplicato per una costante - 2). 
* `extra` **{String}** Caratteri extra da aggiungere al termine della stringa (generalmente `\n` per spaziature verticali)

#### Tipi restituiti

* `graphicString` **{String}** Stringa da utilizzare come header per le funzioni `describe()` di *unittesting* 

**Se `process.env.DEBUG` è `true` o `1` aggiunge un `\n` all'inizio della stringa per aumentare la spaziatura verticale**

[Torna al Sommario](#summary)

<br>

___

<a name='getheader'></a> 

### `leukos-tech-jsutils.tests.getHeader(header {String}, level {Number} ): graphicString {String}`

Restituisce una stringa grafica per l'header fornito, in base al preset selezionato

#### Argomenti

* `header` **{String}** Stringa con l'header
* `level` **{Number}** Identificativo del tipo di formattazione (default=2)


#### Tipi restituiti

* `graphicString` **{String}** Stringa da utilizzare come header per le funzioni `describe()` di *unittesting* 

**Se `process.env.DEBUG` è `true` aggiunge un `\n` all'inizio della stringa per aumentare la spaziatura verticale**

#### Valori di `level` e corrispettive formattazioni

* **0** `>` `|======== <header> ===========================|`
* **1** `-->` `:------ <header> -------------------------:`
* **2** `---->` `----- <header> ------------------------` [DEFAULT]
* **3** `---->` `~~~~~ <header ~~~~~~~~~~~~~~~~~~~~~~~~~` 
* **4** `-->` `¦~~~~~~ <header> ~~~~~~~~~~~~~~~~~~~~~~~~~¦`
* **5** `>` `§~~~~~~~~ <header> ~~~~~~~~~~~~~~~~~~~~~~~~~~~§`

[Torna al Sommario](#summary)

___

<a name='timingresults'></a> 

### `leukos-tech-jsutils.tests.TimingResults`

Classe per gestire, mostrare e memorizzare misurazioni temporali in alta risoluzione dell'esecuzione delle funzioni testate.

<br>

#### `TimingResults(header {String})`

Costruttore dell'oggetto

##### Argomenti

* `header` **{String}** stringa da utilizzare come intestazione per il report coi risultati (es. `"myfunction(arg1, arg2)"`)

<br>

#### `TimingResults.add(header {String}, time {Array}, args {Array})`

Aggiunge una misurazione ai risultati (da utilizzare nei metodi `it()`).

##### Argomenti

* `header` **{String}** Signature della funzione testata o id del test case(es. `"myfunction(arg1, arg2)"`)
* `time` **{Array}** Array di 2 elementi prodotto dal metodo `process.hrtime()`
* `args` **{Array}** Array con gli argomenti utilizzati per la chiamata al metodo testato

<br>

#### `TimingResults.getResults(save {Boolean} = true, reset {Boolean} = true)`

Restituisce i risultati sotto forma di stringa multilinea. Se `save` è `true`, li memorizza su disco. Se `reset` è `true` azzera l'oggetto prima di restituirli sotto forma di stringa multilinea.

##### Argomenti

* `save` **{Boolean}** Se `true` (*default*) memorizza i dati su disco nella posizione `{%HOME}/Leukos-Tech-TimingResults/{header}/{isoDate}.txt`, altrimenti si limita a visualizzare il report a schermo.
* `reset` **{Boolean}** Se `true` (*default*) azzera i dati nell'oggetto prima di restituirli sotto forma di stringa multilinea, altrimenti li matiene.

##### Tipi Restituiti

* `resultsString` **{String}** Stringa multilinea da passare alla `console` in fase di *unittesting*.

[Torna al Sommario](#summary)

<br><br><br>

___

<a name='decorators'></a> 

## `leukos-tech-jsutils.decorators`

Decoratori per funzioni e metodi suddivisi per categoria

<br>

<a name='cacheit'></a> 

### `leukos-tech-jsutils.decorators.cacheIt (func {Function}, enabled {Boolean} = true)`

Sottopone a *caching* il metodo/funzione decorato/a.

#### Argomenti

* `method` **{Function}** Funzione o metodo cui applicare il *caching*.

* `enabled` **{Boolean}** Se `true` (*default*) la funzionalità di *caching* viene abilitata.

#### Tipi restituiti

* `cachedFunc` **{Function}** La funzione sottoposta a *caching*.

#### Utilizzo

Notazione standard

```javascript
let cachedMethod = leukos-tech-jsutils.decorators.cacheIt(method, true);
```

Notazione decoratore
```javascript
@leukos-tech-jsutils.decorators.cacheIt(true)
function laMiaFunzione(args) {
    ...
}
```

[Torna al Sommario](#summary)

<br><br><br>

___

<a name='validators'></a> 

## `leukos-tech-jsutils.validators`

Funzioni di verifica per gli oggetti.

<br>

<a name='validateobject'></a> 

### `leukos-tech-jsutils.validators.validateObject(inspected, cfrData)`

Verifica che l'oggetto `inspected` fornito come argomento possieda le proprietà specificate nell'oggetto `cfrData`.

#### Argomenti

* `inspected` **{Object}** Oggetto da validare.

* `cfrData` **{Object}** Oggetto con i dati per la verifica

    La struttura deve essere la seguente:

    `{ <propertyName>: <propertyType>, <propertyName>: <propertyType>, ...}`

    es.

    `{ "name": "string", "phone": "number", ... }`

    Se al posto della stringa con tipo atteso viene fornito `null` il type-check viene saltato, e la sola presenza della proprietà viene assicurata.

#### Tipi restituiti

Restituisce un oggetto con le seguenti proprietà:

* `success` **{Boolean}** `true` se la validazione è stata superata.

* `problem` **{String}** Se `success` è `false` indica la natura del primo problema incontrato dalla funzione (`"MISS"` per una proprietà mancante, `"WRONG"` per una con type errato);

* `propertyName` **{String}** Se `success` è `false` contiene il nome della proprietà che non ha passato il test.

* `value` **{Any}** Se `success==false` e `problem=="WRONG"` contiene il valore (errato) fornito per la proprietà `propertyName`.

* `expected` **{Any}** Se `success==false` e `problem=="WRONG"` contiene il `type` del valore atteso.

[Torna al Sommario](#summary)

<br><br><br>


___

<a name='typeextensions'></a> 

# ESTENSIONE DEI `type` DI BASE

Importare il package equivale automaticamente ad arricchiere gli oggetti di base delle seguenti funzionalità

<a name='stringte'></a> 

## `String`

<a name='stringreplaceall'></a> 

### `String.prototype.replaceAll(str1, str2, ignore)`

Rimpiazza tutte le occorrenze della stringa `str1` con la stringa `str2`, in modalità `case-unsensitive` se l'argomento `ignore` viene fornito `true`

#### Argomenti

* `str1` **{String}** Porzione della stringa da rimpiazzare
* `str2` **{String}** Stringa con cui rimpiazzare `str1` nella stringa
* `ignore` **{Boolean}** Se `true` ignora il case. Altrimenti (*default*) lo tiene in cosiderazione.

#### Utilizzo

```javascript
require('leukos-tech-jsutils'); // L'estensione dei types avviene qui.

"ABABAB".replaceAll("A", "C") ==> "CBCBCB";

"x".replaceAll("x", "xyz") ==> "xyz"

"x".replaceAll("", "xyz") ==> "xyzxxyz"

"aA".replaceAll("a", "b", true) ==> "bb"

"Hello???".replaceAll("?", "!") ==> "Hello!!!""

```

<br>

[Torna al Sommario](summary)


