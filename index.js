/** Leukos-Tech-JSUtils
 * 
 *  v. 0.2.5
 * 
 * contiene gli oggetti
 * 
 * * `json` Contiene funzioni correlate all'oggetto `JSON`
 * 
 * * `io` Contiene funzioni correlate alle operazioni di I/O
 * 
 * * `log` Contiene funzioni di utilità per il logging
 * 
 * * `error` Contiene funzioni dedicate agli errori e alla loro gestione
 * 
 * * `ancestors` Contiene classi da ereditare per acquisire funzionalità specifiche
 * 
 * * `custErrors` Classi di errore personalizzate per *Leukos-Tech*
 * 
 * * `tests` Funzioni dedicate allo unittesting (es. formattazione headers)
 * 
 * * `decorators` Decoratori per conferire alle funzioni capacità addizionali
 * 
 * Estende gli oggetti con i tipi di base (`String`, `Number`, ...) con metodi *customizzati*
 */


require(`./lib/types_extensions`);

const ancestors = require('./lib/ancestors')
const json = require('./lib/json');
const log = require('./lib/log');
const errors = require('./lib/errFunctions');
const customErrors = require('./lib/Errors');
const io = require('./lib/io');
const tests = require('./lib/tests');
const decorators = require(`./lib/decorators`);
const validators = require('./lib/validators');

/**
 * Leukos-Tech-JSUtils 
 * @property {Object} `json` Funzioni legate all'oggetto `JSON`
 * @property {Object} `io` Funzioni generiche di I-O
 * @property {Object} `log` Funzioni di utilità per il logging
 * @property {Object} `errors` Funzioni dedicate egli errori e loro gestione
 * @property {Object} `ancestors` Classi Ancestor da ereditare
 * @property {Object} `custErrors` Classi di Errore personalizzate da utilizzare nei progetti `Leukos-Tech`
 * @property {Object} `tests` Funzioni dedicate allo unittesting
 * @property {Object} `decorators` Decoratori per funzioni e metodi
 */
module.exports = {

    /** @property Funzioni legate all'oggetto `JSON` */
    json: json,

    /** @property Funzioni generiche di I-O */
    io: io,

    /** @property Funzioni di utilità per il logging */
    log: log,

    /** @property Funzioni dedicate egli errori e loro gestione */
    errors: errors,

    /** @property Classi Ancestor da ereditare */
    ancestors: ancestors,
    
    /** @property Classi di Errore personalizzate da utilizzare nei progetti `Leukos-Tech` */
    custErrors: customErrors,

    /** @property Funzioni dedicate allo unittesting */
    tests: tests,

    /** @property Decoratori per funzioni e metodi divisi per categoria di funzionalità */
    decorators: decorators,

    /** @property Validatori di oggetti */
    validators: validators
};


